import React, { useState, useEffect, ChangeEvent } from 'react';
import './App.css';

interface Country {
    country:string
  , countryInfo:{flag:string}
  , population: number
  , todayCases: number
  , deaths: number
}

function CountryItem(props:{country:Country, key:number}) {

  return (
    <li>
      <img src={props.country.countryInfo.flag} alt="flag" width="50px"/> 
      {props.country.country}({props.country.todayCases/props.country.population*1e5})
    </li>);
}

function CountryList(props:{filter:string}) {

  const [countries, setCountries] = useState([] as Country[]);

  function loadCountries() {
    fetch("https://corona.lmao.ninja/v3/covid-19/countries?sort=cases&yesterday=true")
    .then( data => data.json() )
    .then( data => setCountries(data) )
  }
  useEffect(loadCountries, [])

  return (
    <ul>
      { countries
        .filter((c)=>c.country.includes(props.filter))
        .map((c,i) => <CountryItem country={c} key={i}/>) 
      }
    </ul>)
}

function FilterForm(props:{value:string, setFilter:(s:string) => void}) {

  function handleChange(e:ChangeEvent<HTMLInputElement>) {
    props.setFilter(e.target.value)
  }

  return <form>
    <input value={props.value} onChange={handleChange}/>
  </form>
}


function RegisterForm(props:{visible:boolean}) {

  const [username, setUserName] = useState("")
  const [password, setPassword] = useState("")
  const [open, setOpen] = useState(props.visible)

  console.log(`${username} / ${password}`)

  function handleChangeUserName(e:ChangeEvent<HTMLInputElement>) {
    setUserName(e.target.value)
  }
  function handleChangePassword(e:ChangeEvent<HTMLInputElement>) {
    setPassword(e.target.value)
  }

  function openForm() {
    setOpen(true)
  }

  function register() {
    setOpen(false)
  }

  function cancel() {
    setOpen(false)
  }

  return <> { open && 
      <form onSubmit={(e:any) => {e.preventDefault()}}>
        <div>Username: <input type={"text"} value={username} onChange={handleChangeUserName}/></div>
        <div>Password: <input type={"password"} value={password} onChange={handleChangePassword}/></div>
        <button onClick={register}>Register</button>
        <button onClick={cancel}>Cancel</button>
      </form>} 
      { ! open && <button onClick={openForm}>Register</button>}
    </>
}


function App() {
  const [filter, setFilter] = useState("")

  return (
    <div>
    <RegisterForm visible={false}/>
    <header> List of countries </header>
    <FilterForm value={filter} setFilter={setFilter}/>
    <CountryList filter={filter}/>
    </div>
  );
}

export default App;
